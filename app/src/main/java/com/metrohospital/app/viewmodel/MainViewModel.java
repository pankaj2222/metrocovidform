package com.metrohospital.app.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    public MutableLiveData<DataObject> dataObject = new MutableLiveData(new DataObject());

    public void resetData() {
        dataObject.setValue(new DataObject());
    }


    public void updateSection_A_1_and_2(Boolean isFollowUpSample,
                                        Boolean patientInQuarantineFacility,
                                        DataObject.Gender gender,
                                        DataObject.MobileBelongsTo mobileNumberBelongsToSelf,
                                        Boolean downloadedAarogyaSetuApp,
                                        String patientName,
                                        String village,
                                        String district,
                                        String state,
                                        String presentAddress,
                                        String pinCode,
                                        String age,
                                        String mobile,
                                        String nationality) {

        DataObject object = dataObject.getValue();
        DataObject.SectionA sectionA = object.sectionA;
        sectionA.isFollowUpSample = isFollowUpSample;
        DataObject.SectionA.PatientDetails patientDetails = sectionA.patientDetails;
        patientDetails.patientInQuarantineFacility = patientInQuarantineFacility;
        patientDetails.gender = gender;
        patientDetails.mobileNumberBelongsToSelf = mobileNumberBelongsToSelf;
        patientDetails.downloadedAarogyaSetuApp = downloadedAarogyaSetuApp;
        patientDetails.patientName = patientName;
        patientDetails.presentVillageOrTown = village;
        patientDetails.districtOfPresentResident = district;
        patientDetails.stateOfPresentResident = state;
        patientDetails.presentPatientAddress = presentAddress;
        patientDetails.pinCode = pinCode;
        patientDetails.age = age;
        patientDetails.mobileNumber = mobile;
        patientDetails.nationality = nationality;
        dataObject.setValue(object);
    }

    public void updateSection_A_4(boolean cb1,
                                  boolean cb2,
                                  boolean cb3,
                                  boolean cb4,
                                  boolean cb5a,
                                  boolean cb5b,
                                  boolean cb6,
                                  boolean cb7,
                                  boolean cb8,
                                  boolean cb9,
                                  boolean cb10) {
        DataObject object = dataObject.getValue();
        DataObject.SectionA.PatientCategory patientCategory = object.sectionA.patientCategory;
        patientCategory.cat1 = cb1;
        patientCategory.cat2 = cb2;
        patientCategory.cat3 = cb3;
        patientCategory.cat4 = cb4;
        patientCategory.cat5a = cb5a;
        patientCategory.cat5b = cb5b;
        patientCategory.cat6 = cb6;
        patientCategory.cat7 = cb7;
        patientCategory.cat8 = cb8;
        patientCategory.cat9 = cb9;
        patientCategory.cat10 = cb10;
        dataObject.setValue(object);

    }

    public void updateSection_B_1(Boolean symptoms, boolean cough, boolean breathlessness,
                                  boolean soreThroat, boolean diarrhoea, boolean nausea,
                                  boolean chestPain, boolean vomiting, boolean haemoptysis,
                                  boolean nasalDischarge, boolean feverAtEvaluation,
                                  boolean bodyAche, boolean sputum, boolean abdominalPain) {

        DataObject object = dataObject.getValue();
        DataObject.SectionB.ClinicalSymptomsAndSigns clinicalSymptomsAndSigns = object.sectionB.clinicalSymptomsAndSigns;

        clinicalSymptomsAndSigns.symptoms = symptoms;
        clinicalSymptomsAndSigns.cough = cough;
        clinicalSymptomsAndSigns.breathlessness = breathlessness;
        clinicalSymptomsAndSigns.soreThroat = soreThroat;
        clinicalSymptomsAndSigns.diarrhoea = diarrhoea;
        clinicalSymptomsAndSigns.nausea = nausea;
        clinicalSymptomsAndSigns.chestPain = chestPain;
        clinicalSymptomsAndSigns.vomiting = vomiting;
        clinicalSymptomsAndSigns.haemoptysis = haemoptysis;
        clinicalSymptomsAndSigns.nasalDischarge = nasalDischarge;
        clinicalSymptomsAndSigns.feverAtEvaluation = feverAtEvaluation;
        clinicalSymptomsAndSigns.bodyAche = bodyAche;
        clinicalSymptomsAndSigns.sputum = sputum;
        clinicalSymptomsAndSigns.abdominalPain = abdominalPain;

    }


    public void updateSection_B_2(boolean chronicLungDisease, boolean chronicRenalDisease,
                                  boolean immunocompromisedCondition, boolean malignancy,
                                  boolean diabetes, boolean heartDisease, boolean hyperTension,
                                  boolean chronicCondition) {
        DataObject object = dataObject.getValue();
        DataObject.SectionB.PreExistingMedicalCondition preExistingMedicalCondition = object.sectionB.preExistingMedicalCondition;
        preExistingMedicalCondition.chronicLungDisease = chronicLungDisease;
        preExistingMedicalCondition.chronicRenalDisease = chronicRenalDisease;
        preExistingMedicalCondition.immunocompromisedCondition = immunocompromisedCondition;
        preExistingMedicalCondition.malignancy = malignancy;
        preExistingMedicalCondition.diabetes = diabetes;
        preExistingMedicalCondition.heartDisease = heartDisease;
        preExistingMedicalCondition.hyperTension = hyperTension;
        preExistingMedicalCondition.chronicCondition = chronicCondition;
    }


    public void updateSection_B_3(Boolean hospitalized,
                                  String hospitalIdOrNumber,
                                  String hospitalizationDate,
                                  String hospitalState,
                                  String hospitalDistrict,
                                  String hospitalName) {

        DataObject object = dataObject.getValue();
        DataObject.SectionB.HospitalizationDetails hospitalizationDetails = object.sectionB.hospitalizationDetails;
        hospitalizationDetails.hospitalized = hospitalized;
        hospitalizationDetails.hospitalIdOrNumber = hospitalIdOrNumber;
        hospitalizationDetails.hospitalizationDate = hospitalizationDate;
        hospitalizationDetails.hospitalState = hospitalState;
        hospitalizationDetails.hospitalDistrict = hospitalDistrict;
        hospitalizationDetails.hospitalName = hospitalName;
    }

}