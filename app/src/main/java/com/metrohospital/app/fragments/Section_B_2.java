package com.metrohospital.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.metrohospital.app.R;
import com.metrohospital.app.viewmodel.DataObject;
import com.metrohospital.app.viewmodel.MainViewModel;

public class Section_B_2 extends Fragment {

    private MainViewModel mViewModel;
    private CheckBox chronicLungDisease, chronicRenalDisease, immunocompromisedCondition,
            malignancy, diabetes, heartDisease, hyperTension, chronicCondition;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_b_2, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        chronicLungDisease = view.findViewById(R.id.chronicLungDisease);
        chronicRenalDisease = view.findViewById(R.id.chronicRenalDisease);
        immunocompromisedCondition = view.findViewById(R.id.immunocompromisedCondition);
        malignancy = view.findViewById(R.id.malignancy);
        diabetes = view.findViewById(R.id.diabetes);
        heartDisease = view.findViewById(R.id.heartDisease);
        hyperTension = view.findViewById(R.id.hyperTension);
        chronicCondition = view.findViewById(R.id.chronicCondition);

        view.findViewById(R.id.actionPrev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_2.this)
                        .navigate(R.id.action_Section_B_2_TO_Section_B_1);
            }
        });


        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_2.this)
                        .navigate(R.id.action_Section_B_2_TO_Section_B_3);
            }
        });

        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
            @Override
            public void onChanged(DataObject dataObject) {
                bindData(dataObject);
            }
        });
    }

    private void bindData(DataObject dataObject) {
        DataObject.SectionB.PreExistingMedicalCondition preExistingMedicalCondition = dataObject.sectionB.preExistingMedicalCondition;
        chronicLungDisease.setChecked(preExistingMedicalCondition.chronicLungDisease);
        chronicRenalDisease.setChecked(preExistingMedicalCondition.chronicRenalDisease);
        immunocompromisedCondition.setChecked(preExistingMedicalCondition.immunocompromisedCondition);
        malignancy.setChecked(preExistingMedicalCondition.malignancy);
        diabetes.setChecked(preExistingMedicalCondition.diabetes);
        heartDisease.setChecked(preExistingMedicalCondition.heartDisease);
        hyperTension.setChecked(preExistingMedicalCondition.hyperTension);
        chronicCondition.setChecked(preExistingMedicalCondition.chronicCondition);
    }

    private void updateDataObject() {
        mViewModel.updateSection_B_2(chronicLungDisease.isChecked(), chronicRenalDisease.isChecked(),
                immunocompromisedCondition.isChecked(), malignancy.isChecked(), diabetes.isChecked(),
                heartDisease.isChecked(), hyperTension.isChecked(), chronicCondition.isChecked());
    }
}