package com.metrohospital.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.metrohospital.app.R;
import com.metrohospital.app.viewmodel.DataObject;
import com.metrohospital.app.viewmodel.MainViewModel;

public class Section_A_1_and_2 extends Fragment {

    private MainViewModel mViewModel;
    private RadioGroup followUpSampleRV, inQuarantineRV, genderRV, mobileBelongsToRV, aarogyaSetuRV;
    private EditText patientNameET, villageET, districtET, stateET, presentAddressET, pinCodeET, ageET, mobileET, nationalityET;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_a_1_and_2, container, false);
    }

    public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        followUpSampleRV = view.findViewById(R.id.followUpSampleRV);
        inQuarantineRV = view.findViewById(R.id.patientInQuarantine);
        genderRV = view.findViewById(R.id.genderRB);
        mobileBelongsToRV = view.findViewById(R.id.mobileBelongsTo);
        aarogyaSetuRV = view.findViewById(R.id.aarogyaSetuApp);


        patientNameET = view.findViewById(R.id.patientNameET);
        villageET = view.findViewById(R.id.villageET);
        districtET = view.findViewById(R.id.districtET);
        stateET = view.findViewById(R.id.stateET);
        presentAddressET = view.findViewById(R.id.presentAddressET);
        pinCodeET = view.findViewById(R.id.pinET);
        ageET = view.findViewById(R.id.ageET);
        mobileET = view.findViewById(R.id.mobileET);
        nationalityET = view.findViewById(R.id.nationalityET);

        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                updateDataObject();
            }
        });


        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
            @Override
            public void onChanged(DataObject dataObject) {
                bindData(dataObject);
            }
        });
    }


    private void updateDataObject() {
        int checkedFollowUpId = followUpSampleRV.getCheckedRadioButtonId();
        int checkedQuarantineId = inQuarantineRV.getCheckedRadioButtonId();
        int checkedGender = genderRV.getCheckedRadioButtonId();
        int checkedMobileBelongsToId = mobileBelongsToRV.getCheckedRadioButtonId();
        int checkedAarogyaSetuApp = aarogyaSetuRV.getCheckedRadioButtonId();

        Boolean isFollowUpSample = checkedFollowUpId == -1 ? null
                : checkedFollowUpId == R.id.yes;
        Boolean patientInQuarantineFacility = checkedQuarantineId == -1 ? null
                : checkedFollowUpId == R.id.quarantine_yes;
        DataObject.Gender gender = checkedGender == -1 ? DataObject.Gender.NONE
                : (checkedGender == R.id.male ? DataObject.Gender.MALE
                : (checkedGender == R.id.female ? DataObject.Gender.FEMALE
                : DataObject.Gender.OTHERS));
        DataObject.MobileBelongsTo mobileNumberBelongsToSelf = checkedMobileBelongsToId == -1
                ? DataObject.MobileBelongsTo.NONE
                : (checkedMobileBelongsToId == R.id.self ? DataObject.MobileBelongsTo.SELF
                : DataObject.MobileBelongsTo.FAMILY);
        Boolean downloadedAarogyaSetuApp = checkedAarogyaSetuApp == -1 ? null
                : checkedAarogyaSetuApp == R.id.downloaded;

        String patientName = patientNameET.getText().toString();
        String village = villageET.getText().toString();
        String district = districtET.getText().toString();
        String state = stateET.getText().toString();
        String presentAddress = presentAddressET.getText().toString();
        String pinCode = pinCodeET.getText().toString();
        String age = ageET.getText().toString();
        String mobile = mobileET.getText().toString();
        String nationality = nationalityET.getText().toString();


        mViewModel.updateSection_A_1_and_2(
                isFollowUpSample,
                patientInQuarantineFacility,
                gender,
                mobileNumberBelongsToSelf,
                downloadedAarogyaSetuApp,
                patientName,
                village,
                district,
                state,
                presentAddress,
                pinCode,
                age,
                mobile,
                nationality);


        NavHostFragment.findNavController(Section_A_1_and_2.this)
                .navigate(R.id.action_Section_A_1_and_2_TO_Section_A_4);
    }


    private void bindData(DataObject dataObject) {
        DataObject.SectionA sectionA = dataObject.sectionA;
        DataObject.SectionA.PatientDetails patientDetails = sectionA.patientDetails;


        if (sectionA.isFollowUpSample != null) {
            followUpSampleRV.check(sectionA.isFollowUpSample ? R.id.yes : R.id.no);
        } else {
            followUpSampleRV.clearCheck();
        }

        if (patientDetails.patientInQuarantineFacility != null) {
            inQuarantineRV.check(patientDetails.patientInQuarantineFacility
                    ? R.id.quarantine_yes : R.id.quarantine_no);
        } else {
            inQuarantineRV.clearCheck();
        }

        if (patientDetails.gender != DataObject.Gender.NONE) {
            switch (patientDetails.gender) {
                case MALE:
                    genderRV.check(R.id.male);
                    break;
                case FEMALE:
                    genderRV.check(R.id.female);
                    break;
                case OTHERS:
                    genderRV.check(R.id.others);
                    break;
            }
        } else {
            genderRV.clearCheck();
        }

        if (patientDetails.mobileNumberBelongsToSelf != DataObject.MobileBelongsTo.NONE) {
            mobileBelongsToRV.check(patientDetails.mobileNumberBelongsToSelf ==
                    DataObject.MobileBelongsTo.SELF ? R.id.self : R.id.family);
        } else {
            mobileBelongsToRV.clearCheck();
        }

        if (patientDetails.downloadedAarogyaSetuApp != null) {
            aarogyaSetuRV.check(patientDetails.downloadedAarogyaSetuApp
                    ? R.id.downloaded : R.id.notDownloaded);
        } else {
            aarogyaSetuRV.clearCheck();
        }


        patientNameET.setText(patientDetails.patientName);
        villageET.setText(patientDetails.presentVillageOrTown);
        districtET.setText(patientDetails.districtOfPresentResident);
        stateET.setText(patientDetails.stateOfPresentResident);
        presentAddressET.setText(patientDetails.presentPatientAddress);
        pinCodeET.setText(patientDetails.pinCode);
        ageET.setText(patientDetails.age);
        mobileET.setText(patientDetails.mobileNumber);
        nationalityET.setText(patientDetails.nationality);

    }

}