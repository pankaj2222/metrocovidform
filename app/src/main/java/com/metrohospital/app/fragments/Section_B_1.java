package com.metrohospital.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.metrohospital.app.R;
import com.metrohospital.app.viewmodel.DataObject;
import com.metrohospital.app.viewmodel.MainViewModel;

public class Section_B_1 extends Fragment {

    private MainViewModel mViewModel;
    private CheckBox cough, breathlessness, soreThroat, diarrhoea, nausea, chestPain, vomiting,
            haemoptysis, nasalDischarge, feverAtEvaluation, bodyAche, sputum, abdominalPain;
    private LinearLayout symptomsLL;
    private RadioGroup symptomsRG;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_b_1, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        symptomsLL = view.findViewById(R.id.symptomsLL);
        symptomsRG = view.findViewById(R.id.symptomsRG);

        cough = view.findViewById(R.id.cough);
        breathlessness = view.findViewById(R.id.breathlessness);
        soreThroat = view.findViewById(R.id.soreThroat);
        diarrhoea = view.findViewById(R.id.diarrhoea);
        nausea = view.findViewById(R.id.nausea);
        chestPain = view.findViewById(R.id.chestPain);
        vomiting = view.findViewById(R.id.vomiting);
        haemoptysis = view.findViewById(R.id.haemoptysis);
        nasalDischarge = view.findViewById(R.id.nasalDischarge);
        feverAtEvaluation = view.findViewById(R.id.feverAtEvaluation);
        bodyAche = view.findViewById(R.id.bodyAche);
        sputum = view.findViewById(R.id.sputum);
        abdominalPain = view.findViewById(R.id.abdominalPain);

        symptomsRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                symptomsLL.setVisibility(checkedId == R.id.yes ? View.VISIBLE : View.GONE);
            }
        });

        view.findViewById(R.id.actionPrev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_1.this)
                        .navigate(R.id.action_Section_B_1_TO_Section_A_4);
            }
        });


        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_1.this)
                        .navigate(R.id.action_Section_B_1_TO_Section_B_2);
            }
        });

        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
            @Override
            public void onChanged(DataObject dataObject) {
                bindData(dataObject);
            }
        });
    }

    private void bindData(DataObject dataObject) {
        DataObject.SectionB.ClinicalSymptomsAndSigns clinicalSymptomsAndSigns = dataObject.sectionB.clinicalSymptomsAndSigns;

        if (clinicalSymptomsAndSigns.symptoms != null) {
            symptomsRG.check(clinicalSymptomsAndSigns.symptoms ? R.id.yes : R.id.no);
            symptomsLL.setVisibility(clinicalSymptomsAndSigns.symptoms ? View.VISIBLE : View.GONE);
        } else {
            symptomsRG.clearCheck();
            symptomsLL.setVisibility(View.GONE);
        }

        cough.setChecked(clinicalSymptomsAndSigns.cough);
        breathlessness.setChecked(clinicalSymptomsAndSigns.breathlessness);
        soreThroat.setChecked(clinicalSymptomsAndSigns.soreThroat);
        diarrhoea.setChecked(clinicalSymptomsAndSigns.diarrhoea);
        nausea.setChecked(clinicalSymptomsAndSigns.nausea);
        chestPain.setChecked(clinicalSymptomsAndSigns.chestPain);
        vomiting.setChecked(clinicalSymptomsAndSigns.vomiting);
        haemoptysis.setChecked(clinicalSymptomsAndSigns.haemoptysis);
        nasalDischarge.setChecked(clinicalSymptomsAndSigns.nasalDischarge);
        feverAtEvaluation.setChecked(clinicalSymptomsAndSigns.feverAtEvaluation);
        bodyAche.setChecked(clinicalSymptomsAndSigns.bodyAche);
        sputum.setChecked(clinicalSymptomsAndSigns.sputum);
        abdominalPain.setChecked(clinicalSymptomsAndSigns.abdominalPain);
    }

    private void updateDataObject() {
        int checkedSymptomsId = symptomsRG.getCheckedRadioButtonId();
        if (checkedSymptomsId == -1 || checkedSymptomsId == R.id.no) {
            mViewModel.updateSection_B_1(checkedSymptomsId == -1 ? null : false, false, false,
                    false, false, false, false, false, false, false, false, false, false, false);
        } else {
            mViewModel.updateSection_B_1(true, cough.isChecked(), breathlessness.isChecked(),
                    soreThroat.isChecked(), diarrhoea.isChecked(), nausea.isChecked(),
                    chestPain.isChecked(), vomiting.isChecked(), haemoptysis.isChecked(),
                    nasalDischarge.isChecked(), feverAtEvaluation.isChecked(),
                    bodyAche.isChecked(), sputum.isChecked(), abdominalPain.isChecked());
        }


    }
}