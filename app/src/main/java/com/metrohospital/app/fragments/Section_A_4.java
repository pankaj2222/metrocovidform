package com.metrohospital.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.metrohospital.app.R;
import com.metrohospital.app.viewmodel.DataObject;
import com.metrohospital.app.viewmodel.MainViewModel;

public class Section_A_4 extends Fragment {

    private MainViewModel mViewModel;
    private CheckBox cb1, cb2, cb3, cb4, cb5a, cb5b, cb6, cb7, cb8, cb9, cb10;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_a_4, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        cb1 = view.findViewById(R.id.cb1);
        cb2 = view.findViewById(R.id.cb2);
        cb3 = view.findViewById(R.id.cb3);
        cb4 = view.findViewById(R.id.cb4);
        cb5a = view.findViewById(R.id.cb5a);
        cb5b = view.findViewById(R.id.cb5b);
        cb6 = view.findViewById(R.id.cb6);
        cb7 = view.findViewById(R.id.cb7);
        cb8 = view.findViewById(R.id.cb8);
        cb9 = view.findViewById(R.id.cb9);
        cb10 = view.findViewById(R.id.cb10);


        view.findViewById(R.id.actionPrev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_A_4.this)
                        .navigate(R.id.action_Section_A_4_TO_Section_A_1_and_2);
            }
        });

        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_A_4.this)
                        .navigate(R.id.action_Section_A_4_TO_Section_B_1);
            }
        });

        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
            @Override
            public void onChanged(DataObject dataObject) {
                DataObject.SectionA.PatientCategory patientCategory = dataObject.sectionA.patientCategory;
                cb1.setChecked(patientCategory.cat1);
                cb2.setChecked(patientCategory.cat2);
                cb3.setChecked(patientCategory.cat3);
                cb4.setChecked(patientCategory.cat4);
                cb5a.setChecked(patientCategory.cat5a);
                cb5b.setChecked(patientCategory.cat5b);
                cb6.setChecked(patientCategory.cat6);
                cb7.setChecked(patientCategory.cat7);
                cb8.setChecked(patientCategory.cat8);
                cb9.setChecked(patientCategory.cat9);
                cb10.setChecked(patientCategory.cat10);
            }
        });
    }

    private void updateDataObject() {
        mViewModel.updateSection_A_4(cb1.isChecked(), cb2.isChecked(), cb3.isChecked(),
                cb4.isChecked(), cb5a.isChecked(), cb5b.isChecked(), cb6.isChecked(),
                cb7.isChecked(), cb8.isChecked(), cb9.isChecked(), cb10.isChecked());
    }
}