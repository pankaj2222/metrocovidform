package com.metrohospital.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.metrohospital.app.R;
import com.metrohospital.app.viewmodel.DataObject;
import com.metrohospital.app.viewmodel.MainViewModel;

public class Section_B_3 extends Fragment {

    private MainViewModel mViewModel;
    private EditText hospitalIdOrNumberET, hospitalizationDateET,
            hospitalStateET, hospitalDistrictET, hospitalNameET;
    private LinearLayout hospitalizedLL;
    private RadioGroup hospitalizedRG;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_b_3, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        hospitalizedLL = view.findViewById(R.id.hospitalizedLL);
        hospitalizedRG = view.findViewById(R.id.hospitalizedRG);

        hospitalIdOrNumberET = view.findViewById(R.id.hospitalIdOrNumber);
        hospitalizationDateET = view.findViewById(R.id.hospitalizationDate);
        hospitalStateET = view.findViewById(R.id.hospitalState);
        hospitalDistrictET = view.findViewById(R.id.hospitalDistrict);
        hospitalNameET = view.findViewById(R.id.hospitalName);

        hospitalizedRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                hospitalizedLL.setVisibility(checkedId == R.id.yes ? View.VISIBLE : View.GONE);
            }
        });

        view.findViewById(R.id.actionPrev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_3.this)
                        .navigate(R.id.action_Section_B_3_TO_B_2);
            }
        });


        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataObject();
                NavHostFragment.findNavController(Section_B_3.this)
                        .navigate(R.id.action_Section_B_3_TO_Review_Form);
            }
        });

        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
            @Override
            public void onChanged(DataObject dataObject) {
                bindData(dataObject);
            }
        });
    }

    private void bindData(DataObject dataObject) {
        DataObject.SectionB.HospitalizationDetails hospitalizationDetails = dataObject.sectionB.hospitalizationDetails;

        if (hospitalizationDetails.hospitalized != null) {
            hospitalizedRG.check(hospitalizationDetails.hospitalized ? R.id.yes : R.id.no);
            hospitalizedLL.setVisibility(hospitalizationDetails.hospitalized ? View.VISIBLE : View.GONE);
        } else {
            hospitalizedRG.clearCheck();
            hospitalizedLL.setVisibility(View.GONE);
        }

        hospitalIdOrNumberET.setText(hospitalizationDetails.hospitalIdOrNumber);
        hospitalizationDateET.setText(hospitalizationDetails.hospitalizationDate);
        hospitalStateET.setText(hospitalizationDetails.hospitalState);
        hospitalDistrictET.setText(hospitalizationDetails.hospitalDistrict);
        hospitalNameET.setText(hospitalizationDetails.hospitalName);
    }

    private void updateDataObject() {
        int checkedHospitalizationId = hospitalizedRG.getCheckedRadioButtonId();
        if(checkedHospitalizationId == -1 || checkedHospitalizationId == R.id.no) {
            mViewModel.updateSection_B_3(checkedHospitalizationId == -1 ? null : false,
                    null, null, null, null, null);
        } else {
            mViewModel.updateSection_B_3(true, hospitalIdOrNumberET.getText().toString(),
                    hospitalizationDateET.getText().toString(), hospitalStateET.getText().toString(),
                    hospitalDistrictET.getText().toString(), hospitalNameET.getText().toString());
        }

    }
}