package com.metrohospital.app.retrofit;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface InterfaceApi {
    /* @GET url is after Base_URL.
       We are going to get List of country as response.
    */



    @POST("/pharma/admin/WEBSERVICES/savecovid")
    @FormUrlEncoded
    Call<StatusModel> savecovid(@Field("APIKEY") String APIKEY,
                               @Field("covid_name") String covid_name,
                               @Field("covid_age") String covid_age,
                               @Field("covid_gender") String covid_gender,
                               @Field("covid_mobile") String covid_mobile,
                               @Field("covid_add") String covid_add,
                               @Field("covid_category_pt") String covid_category_pt,
                               @Field("b1Symptoms") String b1Symptoms,
                               @Field("covid_sympt_signs") String covid_sympt_signs,
                               @Field("covid_medi_condt") String covid_medi_condt,
                               @Field("covid_imm_con") String covid_imm_con


    );




    @POST("/pharma/admin/WEBSERVICES/verify_otp")
    @FormUrlEncoded
    Call<StatusModel> checkOTP(@Field("APIKEY") String APIKEY,
                               @Field("otpcode") String otpcode,
                               @Field("mobile") String mobile

    );







}