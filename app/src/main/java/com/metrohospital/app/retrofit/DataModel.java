package com.metrohospital.app.retrofit;
import java.util.List;

/**
 * Created by pankaj on 6/7/2017.
 */
public class DataModel {
    public List<Articles> articles;
    public List<Articles> getArticles() {
        return articles;
    }
    public void setArticles(List<Articles> articles) {
        this.articles = articles;
    }
    public class Articles {
        String title;
        String url;
        String description;

        public String getDescription() {
            return description;
        }
        public void setDescription(String description) {
            this.description = description;
        }
        public String getTitle() {
            return title;
        }
        public void setTitle(String title) {
            this.title = title;
        }
        public String getUrl() {
            return url;
        }
        public void setUrl(String url) {
            this.url = url;
        }
    }
}
